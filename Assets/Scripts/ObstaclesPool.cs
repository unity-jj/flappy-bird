using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesPool : MonoBehaviour
{
  [SerializeField] private GameObject obstaclePrefab;
  [SerializeField] private int poolSize = 4;
  [SerializeField] private float spawnTime = 2.5f;
  [SerializeField] private float xSpawnPosition = 8f;
  [SerializeField] private float minYPosition = 0f;
  [SerializeField] private float maxYPosition = 4f;
  private float timeElapsed;
  private int obstacleCount = 0;
  private GameObject[] obstacles;

  void Start()
  {
    obstacles = new GameObject[poolSize];

    for (int i = 0; i < poolSize; i++)
    {
      obstacles[i] = Instantiate(obstaclePrefab);
      obstacles[i].SetActive(false);
    }
  }

  void Update()
  {
    timeElapsed += Time.deltaTime;

    if (timeElapsed > spawnTime && !GameManager.Instance.isGameOver)
    {
      SpawnObstacle();
    }
  }

  private void SpawnObstacle()
  {
    timeElapsed = 0f;

    var obstacle = obstacles[obstacleCount];

    float ySpawnPosition = Random.Range(minYPosition, maxYPosition);
    Vector2 spawnPosition = new Vector2(xSpawnPosition, ySpawnPosition);
    obstacle.transform.position = spawnPosition;

    if (!obstacle.activeSelf)
    {
      obstacle.SetActive(true);
    }

    obstacleCount += 1;

    if (obstacleCount == poolSize)
    {
      obstacleCount = 0;
    }
  }
}
