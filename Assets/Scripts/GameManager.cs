using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine;

public class GameManager : MonoBehaviour
{
  [SerializeField] private GameObject gameOverText;
  [SerializeField] private TMP_Text scoreText;
  public bool isGameOver;
  private int score;
  private static GameManager instance;
  public static GameManager Instance { get { return instance; } }

  void Awake()
  {
    if (instance == null)
    {
      instance = this;
    }
    else
    {
      Destroy(gameObject);
    }
  }

  void Update()
  {
    if (Input.GetMouseButtonDown(0) && isGameOver)
    {
      RestartGame();
    }
  }

  public void GameOver()
  {
    gameOverText.SetActive(true);
    isGameOver = true;
  }

  private void RestartGame()
  {
    SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
  }

  public void IncreaseScore()
  {
    score += 1;
    scoreText.text = score.ToString();
  }
}
