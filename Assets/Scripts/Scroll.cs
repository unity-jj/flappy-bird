using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scroll : MonoBehaviour
{
  [SerializeField]
  private float speed = 2.5f;
  private Rigidbody2D rigidBody;
  void Start()
  {
    rigidBody = GetComponent<Rigidbody2D>();
    rigidBody.velocity = Vector2.left * speed;
  }

  void Update()
  {
    if (GameManager.Instance.isGameOver)
    {
      rigidBody.velocity = Vector2.zero;
    }
  }
}
